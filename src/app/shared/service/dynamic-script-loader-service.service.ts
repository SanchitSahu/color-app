import { Injectable } from '@angular/core';
import { $ } from 'protractor';

interface Scripts {
  name: string;
  src: string;
}

export const ScriptStore: Scripts[] = [

  { name: 'jqueryMIn', src:'../../../assets/js/jquery.min.js' },
  { name: 'pCoded', src:'../../../assets/js/pcoded.min.js' },
  { name: 'commomnJS', src:'../../../assets/js/common-pages.js' },
  { name: 'veriticalJs', src:'../../../assets/js/vertical/vertical-layout.min.js' },
  { name: 'datedropperJs', src: '../../../assets/js/datedropper.min.js'},
  { name: 'daterangeJs', src: '../../../assets/js/daterangepicker.js'},
  { name: 'spectrumJs', src: '../../../assets/js/spectrum.js'},
  { name: 'jqueryminicolors', src: '../../../assets/js/jquery.minicolors.min.js'},
  { name: 'customPickerJs', src: '../../../assets/js/custom-picker.js'},
  { name: 'jqueryDatatables', src: '../../../assets/js/jquery.dataTables.min.js'},
  { name: 'customTableJs', src: '../../../assets/js/data-table-custom.js'}

  // { name: 'jqueryMIn', src:'http://13.127.108.174/colors/assets/js/jquery.min.js' },
  // { name: 'pCoded', src:'http://13.127.108.174/colors/assets/js/pcoded.min.js' },
  // { name: 'commomnJS', src:'http://13.127.108.174/colors/assets/js/common-pages.js' },
  // { name: 'veriticalJs', src:'http://13.127.108.174/colors/assets/js/vertical/vertical-layout.min.js' },
  // { name: 'datedropperJs', src: 'http://13.127.108.174/colors/assets/js/datedropper.min.js'},
  // { name: 'daterangeJs', src: 'http://13.127.108.174/colors/assets/js/daterangepicker.js'},
  // { name: 'spectrumJs', src: 'http://13.127.108.174/colors/assets/js/spectrum.js'},
  // { name: 'jqueryminicolors', src: 'http://13.127.108.174/colors/assets/js/jquery.minicolors.min.js'},
  // { name: 'customPickerJs', src: 'http://13.127.108.174/colors/assets/js/custom-picker.js'},
  // { name: 'jqueryDatatables', src: 'http://13.127.108.174/colors/assets/js/jquery.dataTables.min.js'},
  // { name: 'customTableJs', src: 'http://13.127.108.174/colors/assets/js/data-table-custom.js'}

];

declare var document: any;

@Injectable({
  providedIn: 'root'
})
export class DynamicScriptLoaderServiceService {
  private scripts: any = {};

  constructor() { 
    ScriptStore.forEach((script: any) => {
      this.scripts[script.name] = {
        loaded: false,
        src: script.src
      };
    });
  }

  load(...scripts: string[]) {

    const promises: any[] = [];
    //console.log(promises);
    scripts.forEach((script) => promises.push(this.loadScript(script)));
    return Promise.all(promises);
  }

  loadScript(name: string) {
   // console.log(name);
    return new Promise((resolve, reject) => {
      if (!this.scripts[name].loaded) {
        //load script
        let script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = this.scripts[name].src;
        if (script.readyState) {  //IE
            script.onreadystatechange = () => {
                if (script.readyState === "loaded" || script.readyState === "complete") {
                    script.onreadystatechange = null;
//to make the script.loaded false that means the script will be loaded everytime throughout the page                    
                   // this.scripts[name].loaded = true;
                    resolve({script: name, loaded: true, status: 'Loaded'});
                }
            };
        } else {  //Others
            script.onload = () => {
               // this.scripts[name].loaded = true;
                resolve({script: name, loaded: true, status: 'Loaded'});
            };
        }
        script.onerror = (error: any) => resolve({script: name, loaded: false, status: 'Loaded'});
        document.getElementsByTagName('head')[0].appendChild(script);
      } else {
        resolve({ script: name, loaded: true, status: 'Already Loaded' });
      }
    });
  }

  // customDashboard(){
  //   $(function () {


  //   })
  // }
}