import { Observable } from 'rxjs';
import { Admin } from '../model/admin';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AdminService {

  url = 'http://13.127.108.174:5000/api/v1/users';
  cpurl = 'http://13.127.108.174:5000/api/v1/users/changePassword';
  colorUrl = 'http://13.127.108.174:5000/api/v1/colors/getColors';
  colorShadeUrl = 'http://13.127.108.174:5000/api/v1/colors/getColorShades';
  deleteCustomerUrl = 'http://13.127.108.174:5000/api/v1/users/deleteCustomerDetails';
  deleteContactUsUrl = 'http://13.127.108.174:5000/api/v1/users/deleteContactUs';
  deleteColorUrl = 'http://13.127.108.174:5000/api/v1/colors/deleteColor';
  deleteColorShadeUrl = 'http://13.127.108.174:5000/api/v1/colors/deleteColorShade';
  deleteServiceRequestUrl = 'http://13.127.108.174:5000/api/v1/users/deleteServiceRequest';
  editColorsUrl = 'http://13.127.108.174:5000/api/v1/colors/editColor';
  addServiceRequestUrl = 'http://13.127.108.174:5000/api/v1/users/addServiceRequest';
  addColorUrl = 'http://13.127.108.174:5000/api/v1/colors/addColor';
  addColorShadeUrl = 'http://13.127.108.174:5000/api/v1/colors/addColorShade';
  editColorShadeUrl= 'http://13.127.108.174:5000/api/v1/colors/editColorShade';
  allColorShadesUrl= 'http://13.127.108.174:5000/api/v1/colors/getColorShades';

  constructor(private http: HttpClient) { }

  changePassword(data): Observable<any> {
    // console.log({oldPassword: data.oldPassword, newPassword: data.newPassword, confirmPassword:data.confirmPassword});
    // return this.http.put<any>(`${this.cpurl}`, { });
    let oldPassword = data.oldPassword;
    let newPassword = data.newPassword;
    let confirmPassword = data.confirmPassword;
    console.log({ oldPassword: data.oldPassword, newPassword: data.newPassword, confirmPassword: data.confirmPassword });
    return this.http.put<any>(`${this.cpurl}`, { oldPassword, newPassword, confirmPassword });
  }

  dashboardItems() {
    // console.log();
    return this.http.get(this.url + '/dashboard');
  }

  getCustomerDetails(): Observable<any> {
    return this.http.get<any>(this.url + '/getCustomerDetails');
  }

  getContactUs(): Observable<any> {
    return this.http.get<any>(this.url + '/getContactUs');
  }

  getServiceRequest(): Observable<any> {
    return this.http.get<any>(this.url + '/getServiceRequests');
  }

  getColor(): Observable<any> {
    return this.http.get<any>(this.colorUrl);
  }
  
  getAllColorShade(): Observable<any> {
    console.log();
    return this.http.get<any>(this.allColorShadesUrl);
  } 

// Color Shade by id
  getColorShade(id: any): Observable<any> {
    console.log();
    return this.http.get<any>(this.colorShadeUrl + '/' + id);
  }  
 
  // GET Color by id to edit
  getColorDetails(id: any): Observable<any> {
    console.log(id);
    // const ed = 'edit';
    return this.http.get<any>( this.colorUrl + '/' + id);
  }

  editColor(data): Observable<any> {
    let id = data.id;
    let name = data.name;
    let colorCode = data.colorCode;
    //console.log(data.id, data.name, data.colorCode);
    return this.http.post<any>(this.editColorsUrl, { id, name, colorCode });
  }

  // GET Color Shade by id to edit
  getColorShadeDetails(id: any): Observable<any> {
    console.log(id);
   const sin = 'single';
    return this.http.get<any>( this.colorShadeUrl + '/' + id + '/' + sin);
  }

  editColorShade(data): Observable<any> {
    let id = data.id;
    let colorId = data.colorId;
    let shadeName = data.shadeName;
    let colorCode = data.colorCode;
    //console.log(data.id, data.name, data.colorCode);
    return this.http.post<any>(this.editColorShadeUrl, { id, colorId, shadeName, colorCode });
  }

  deleteCustomerDetails(id: any): Observable<any> {
    //console.log(id);
    return this.http.post<any>(this.deleteCustomerUrl, { id });
  }

  deleteContactUsData(id: any): Observable<any> {
    //console.log(id);
    return this.http.post<any>(this.deleteContactUsUrl, { id });
  }

  deleteServiceRequest(id): Observable<any> {
    //console.log(id);
    return this.http.post<any>(this.deleteServiceRequestUrl, { id });
  }

  deleteColor(id): Observable<any> {
    //console.log(id);
    return this.http.post<any>(this.deleteColorUrl, { id });
  }

  deleteColorShade(id): Observable<any> {
    //console.log(id);
    return this.http.post<any>(this.deleteColorShadeUrl, { id });
  }

  // addServiceRequest(data): Observable<any> {
  //   let name = data.name;
  //   let email = data.email;
  //   let phoneNumber = data.phoneNumber;
  //   let pincode = data.pincode;
  //   let requestType = data.requestType;
  //   //console.log({name: data.name, email: data.email, phoneNumber:data.phoneNumber, pincode:data.pincode, requestType:data.requestType});
  //   return this.http.post<any>(this.addServiceRequestUrl, { name, email, phoneNumber, pincode, requestType });
  // }


  addColor(name, colorCode): Observable<any> {
    // let name = data.name;
    // let colorCode = data.colorCode;
    // console.log(data.name, data.colorCode);
    console.log(name, colorCode);
    return this.http.post<any>(this.addColorUrl, { name, colorCode }); 
    //{} is used to send data in the header
  }
 
  addColorShade(colorId,shadeName, colorCode): Observable<any>{
    console.log(colorId,shadeName, colorCode);
    return this.http.post<any>(this.addColorShadeUrl, {colorId, shadeName, colorCode });
  }
}