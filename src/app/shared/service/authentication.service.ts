import { Admin } from './../model/admin';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {

  url= 'http://13.127.108.174:5000/api/v1/users/login';

    private currentUserSubject: BehaviorSubject<Admin>;
    public currentUser: Observable<Admin>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<Admin>(JSON.parse(localStorage.getItem('currentuser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): Admin {
       // console.log(this.currentUserSubject.value);
        return this.currentUserSubject.value;
    }

    login(username: string, password: string): Observable<any>{
      //console.log(username, password)
         return this.http.post<any>(`${this.url}`, { username, password })
            .pipe(map(admin => {
               // console.log(admin);
                // login successful if there's a jwt token in the response
                if (admin) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                   //console.log(admin);
                    localStorage.setItem('currentuser', JSON.stringify(admin));
                    this.currentUserSubject.next(admin);
                }

                return admin;
            }));
    }
    

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentuser');
        this.currentUserSubject.next(null);
    }
}