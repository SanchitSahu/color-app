import { DynamicScriptLoaderServiceService } from './../../shared/service/dynamic-script-loader-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cms-pages',
  templateUrl: './cms-pages.component.html',
  styleUrls: ['./cms-pages.component.css']
})
export class CmsPagesComponent implements OnInit {

  constructor(private dynamicScriptLoader: DynamicScriptLoaderServiceService) { }

  ngOnInit() {
  }


   //TO LOAD CUSTOM JS DYNAMICALLY

 ngAfterViewInit() {
  this.loadScripts();
}

private loadScripts() {
  // You can load multiple scripts by just providing the key as argument into load method of the service

  this.dynamicScriptLoader.load('jqueryDatatables','customTableJs').then(data => {
    //console.log(data);
    // Script Loaded Successfully
  }).catch(error => console.log(error));
}
}
