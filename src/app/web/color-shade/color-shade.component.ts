import { DynamicScriptLoaderServiceService } from './../../shared/service/dynamic-script-loader-service.service';
import { ActivatedRoute } from '@angular/router';
import { AdminService } from './../../shared/service/admin.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-color-shade',
  templateUrl: './color-shade.component.html',
  styleUrls: ['./color-shade.component.css']
})
export class ColorShadeComponent implements OnInit {

  colorShade: any;
  colorShadedata: any;
  para: any;
  activeId;
  colors: any;
  colorsdata: any;

  constructor(private adminService: AdminService,
    private route: ActivatedRoute,
    private dynamicScriptLoader: DynamicScriptLoaderServiceService) { }

  ngOnInit() {
    this.adminService.getAllColorShade()
      .subscribe(res => {
        //console.log(res);
        this.colorShade = res;
        this.colorShadedata = this.colorShade.data;
        // console.log(this.colorShadedata);
      })
  }

  // CURRENT ID
  deleteColorId(id) {
    this.activeId = id;
    //console.log(this.activeId);
  }
  // DELETE SELECTED COLOR
  deleteColorShade(): void {
    //console.log(this.activeId);  
    this.adminService.deleteColorShade(this.activeId)
      .subscribe(res => {
        //console.log(this.colorsdata);
        this.colorShadedata = this.colorShadedata.filter(cust => cust._id != this.activeId);
        //console.log(this.colorsdata);
      });
  }

  //  TO LOAD CUSTOM JS DYNAMICALLY

  ngAfterViewInit() {
    this.loadScripts();
  }

  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service

    this.dynamicScriptLoader.load('jqueryDatatables', 'customTableJs').then(data => {
      //console.log(data);
      // Script Loaded Successfully
    }).catch(error => console.log(error));
  }
}
