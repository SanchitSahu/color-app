import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartiColorShadeComponent } from './parti-color-shade.component';

describe('PartiColorShadeComponent', () => {
  let component: PartiColorShadeComponent;
  let fixture: ComponentFixture<PartiColorShadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartiColorShadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartiColorShadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
