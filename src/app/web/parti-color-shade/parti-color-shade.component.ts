import { Component, OnInit } from '@angular/core';
import { DynamicScriptLoaderServiceService } from './../../shared/service/dynamic-script-loader-service.service';
import { ActivatedRoute } from '@angular/router';
import { AdminService } from './../../shared/service/admin.service';
@Component({
  selector: 'app-parti-color-shade',
  templateUrl: './parti-color-shade.component.html',
  styleUrls: ['./parti-color-shade.component.css']
})
export class PartiColorShadeComponent implements OnInit {

  activeId:any;
  para: any;
  colorShade: any;
  colorShadedata: any;

  constructor(private adminService: AdminService,
    private route: ActivatedRoute,
    private dynamicScriptLoader: DynamicScriptLoaderServiceService) {route.params.subscribe(value => {
    this.para = this.route.snapshot.paramMap.get('id');
    //console.log(this.para);

    this.adminService.getColorShade(this.para)
      .subscribe(data => {
        //console.log('hello');
        this.colorShade = data;
        this.colorShadedata = this.colorShade.data;
        //console.log(this.jsondata);
      });
  }); 
}

 // CURRENT ID
 deleteColorId(id) {
  this.activeId = id;
  //console.log(this.activeId);
}
// DELETE SELECTED COLOR
deleteColorShade(): void {
  //console.log(this.activeId);  
  this.adminService.deleteColorShade(this.activeId)
    .subscribe(res => {
      //console.log(this.colorsdata);
      this.colorShadedata = this.colorShadedata.filter(cust => cust._id != this.activeId);
      //console.log(this.colorsdata);
    });
}
  ngOnInit() {
  }

}
