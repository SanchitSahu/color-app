import { FormGroup } from '@angular/forms';

  export function MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
        }

        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}


    // static MatchPassword(control: AbstractControl) {
        
    //    let newPassword = control.get('newPassword').value;

    //    let confirmPassword = control.get('confirmPassword').value;

    //     if(newPassword != confirmPassword) {
    //         control.get('confirmPassword').setErrors( {ConfirmPassword: true} );
    //     } else {
    //         return null
    //     }
    // }

// static MatchPassword(control: AbstractControl) {
//     let newPassword = control.get('newPassword').value;
//     let confirmPassword = control.get('confirmPassword').value;
//     console.log(newPassword,confirmPassword);
//     if (newPassword != confirmPassword) {
        
//       return { ConfirmPassword: true };
//     } else {
//         return null
//     }
//   }