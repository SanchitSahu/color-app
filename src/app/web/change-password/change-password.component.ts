import { DynamicScriptLoaderServiceService } from './../../shared/service/dynamic-script-loader-service.service';
//import {MustMatch} from '../../web/change-password/confirmpassword';
//import { ConfirmPasswordValidator } from './confirmpassword';
import { AdminService } from './../../shared/service/admin.service';
import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../shared/service/alert.service';
import { AuthenticationService } from '../../shared/service/authentication.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  changePasswordForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  admin = {oldPassword:'', newPassword: '', confirmPassword: ''};

  constructor(  private dynamicScriptLoader: DynamicScriptLoaderServiceService,
       private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private adminService: AdminService) { }
  
  ngOnInit() {
    this.changePasswordForm = this.formBuilder.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }
  
  get f() { return this.changePasswordForm.controls; }


  updatePassword()
  {
    if(window.confirm('Are you sure, you want to update?')){
    this.adminService.changePassword(this.admin)
     .subscribe(res => {
      //console.log(res);
       if (res.statusCode === 200) { 
           alert("password changed successful");
             console.log('response', res);
             console.log('resp', res.data);
             localStorage.setItem('currentUser', JSON.stringify(res.data));
             this.router.navigate(['/login']);
       }
     },
     );
    }
  }
  ngAfterViewInit() {
    this.loadScripts();
  }
  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service
    this.dynamicScriptLoader.load('commomnJS').then(data => {
      console.log(data);
      // Script Loaded Successfully
     
    }).catch(error => console.log(error));
  }
}
