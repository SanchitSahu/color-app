import { DynamicScriptLoaderServiceService } from './../../shared/service/dynamic-script-loader-service.service';
import { AlertService } from './../../shared/service/alert.service';
import { AdminService } from './../../shared/service/admin.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-color',
  templateUrl: './color.component.html',
  styleUrls: ['./color.component.css']
})
export class ColorComponent implements OnInit {

  colors: any;
  colorsdata: any;
  activeId;

  constructor(private adminService: AdminService,
    private alertService: AlertService,
    private dynamicScriptLoader: DynamicScriptLoaderServiceService) {

  }

  ngOnInit() {
    // Fetch color from API
    this.adminService.getColor()
      .subscribe(color => {
        if (color.statusCode === 200) {
          //console.log(color);
          this.colors = color;
          this.colorsdata = this.colors.data;
          //console.log(this.colorsdata);
        }
      });

  }
  // CURRENT ID
  deleteColorId(id) {
    this.activeId = id;
    //console.log(this.activeId);
  }
  // DELETE SELECTED COLOR
  deleteColor(): void {
    //console.log(this.activeId);  
    this.adminService.deleteColor(this.activeId)
      .subscribe(res => {
        //console.log(this.colorsdata);
        this.colorsdata = this.colorsdata.filter(cust => cust._id != this.activeId);
        //console.log(this.colorsdata);
      },
        error => {
          console.log('error', error);
          this.alertService.error(error);
        });
  }

  // // UPDATE COLOR
  // updateColor(id: String, name: String, colorCode: String): void {
  //   //console.log(id,name,colorCode);
  //   if (window.confirm('Are you sure, you want to update?')) {
  //     this.adminService.editColor({ id, name, colorCode })
  //       .subscribe(data => {
  //         //console.log(data);
  //         this.colorsdata.push(data);
  //       })
  //   }
  // }

  //TO LOAD CUSTOM JS DYNAMICALLY

  ngAfterViewInit() {
    this.loadScripts();
  }

  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service

    this.dynamicScriptLoader.load('jqueryDatatables', 'customTableJs').then(data => {
      //console.log(data);
      // Script Loaded Successfully
    }).catch(error => console.log(error));
  }
}
