import { Component, OnInit } from '@angular/core';
import { DynamicScriptLoaderServiceService } from './../../shared/service/dynamic-script-loader-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from './../../shared/service/admin.service';
import { AlertService } from '../../shared/service/alert.service';
import { AuthenticationService } from '../../shared/service/authentication.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-edit-color-shade',
  templateUrl: './edit-color-shade.component.html',
  styleUrls: ['./edit-color-shade.component.css']
})

export class EditColorShadeComponent implements OnInit {

  para;
  activeId;
  editcolorshade:any;
  editcolorshadedata:any;
  editColorShadeForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor( private dynamicScriptLoader: DynamicScriptLoaderServiceService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private adminService: AdminService,) {
    route.params.subscribe(value => {
      this.para = this.route.snapshot.paramMap.get('id');
     // console.log(this.para);

      this.adminService.getColorShadeDetails(this.para)
        .subscribe(res => {
        //  console.log(res);
          this.editcolorshade = res;
          this.editcolorshadedata= this.editcolorshade.data;
          //console.log(this.editcolordata[0]);
        });
    });
   }

  ngOnInit() {
    this.editColorShadeForm = this.formBuilder.group({
      id: ['', Validators.required],
      colorId: ['', Validators.required],
      name: ['', Validators.required],
      colorCode: ['', Validators.required]
    });
  }

  ngAfterViewInit() {
    this.loadScripts();
  }

  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service
  
      this.dynamicScriptLoader.load('datedropperJs','daterangeJs','jqueryminicolors','customPickerJs','spectrumJs').then(data => {
        //console.log(data);
        // Script Loaded Successfully
      }).catch(error => console.log(error));
  }

get f() { return this.editColorShadeForm.controls; }
// 
updateColorShade(id: number, colorId: number,shadeName: String, colorCode: String): void {
  console.log(id, colorId,shadeName, colorCode);
   if(window.confirm('Are you sure, you want to update?')){
  this.adminService.editColorShade({ id, colorId, shadeName, colorCode})
    .subscribe(data => {
      console.log(data);
      this.editcolorshadedata.push(data);
      this.router.navigate(['/colorshade']);
    })
}
}
}
