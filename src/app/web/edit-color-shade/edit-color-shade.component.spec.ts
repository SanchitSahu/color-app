import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditColorShadeComponent } from './edit-color-shade.component';

describe('EditColorShadeComponent', () => {
  let component: EditColorShadeComponent;
  let fixture: ComponentFixture<EditColorShadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditColorShadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditColorShadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
