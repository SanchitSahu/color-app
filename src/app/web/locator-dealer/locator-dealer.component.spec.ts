import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocatorDealerComponent } from './locator-dealer.component';

describe('LocatorDealerComponent', () => {
  let component: LocatorDealerComponent;
  let fixture: ComponentFixture<LocatorDealerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocatorDealerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocatorDealerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
