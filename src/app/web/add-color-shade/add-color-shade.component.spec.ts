import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddColorShadeComponent } from './add-color-shade.component';

describe('AddColorShadeComponent', () => {
  let component: AddColorShadeComponent;
  let fixture: ComponentFixture<AddColorShadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddColorShadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddColorShadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
