import { Component, OnInit } from '@angular/core';
import { DynamicScriptLoaderServiceService } from './../../shared/service/dynamic-script-loader-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from './../../shared/service/admin.service';
import { AlertService } from '../../shared/service/alert.service';
import { AuthenticationService } from '../../shared/service/authentication.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
@Component({
  selector: 'app-add-color-shade',
  templateUrl: './add-color-shade.component.html',
  styleUrls: ['./add-color-shade.component.css']
})
export class AddColorShadeComponent implements OnInit {

  addColorShadeForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  color=[];
  colors;
  colorsdata;

  constructor( private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private adminService: AdminService,
    private dynamicScriptLoader: DynamicScriptLoaderServiceService) { }

  ngOnInit() {
    this.addColorShadeForm = this.formBuilder.group({
      colorName: ['', Validators.required],
      shadename: ['', Validators.required],
      colorCode: ['', Validators.required]
    });

    this.adminService.getColor()
    .subscribe(color =>{
      if(color.statusCode === 200){
      //console.log(color);
      this.colors = color;
      this.colorsdata = this.colors.data;
      //console.log(this.colorsdata);
      }
    });
  }

  addColorShades(colorId: number,shadeName:string, colorCode:string): void {   
    this.adminService.addColorShade(colorId,shadeName,colorCode)
   .subscribe(res=>{
     console.log('response', res);
     this.color.push(res);
     if (res.statusCode === 200) {
      // console.log('resp', res.data);
                       //alert("Login successful");
                       //localStorage.setItem('currentUser', JSON.stringify(res.data));
                         console.log('Color added successfully');
                         //alert('navigate');
                       this.router.navigate(['/colorshade']);
                   }
    });
   }

   //TO LOAD CUSTOM JS DYNAMICALLY

   ngAfterViewInit() {
    this.loadScripts();
  }

  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service

    this.dynamicScriptLoader.load('datedropperJs', 'daterangeJs', 'jqueryminicolors', 'customPickerJs', 'spectrumJs').then(data => {
      //console.log(data);
      // Script Loaded Successfully
    }).catch(error => console.log(error));
  }

  get f() { return this.addColorShadeForm.controls; }

}
