import { DynamicScriptLoaderServiceService } from './../../shared/service/dynamic-script-loader-service.service';
import { AlertService } from './../../shared/service/alert.service';
import { first } from 'rxjs/operators';
import { AdminService } from './../../shared/service/admin.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})
export class CustomerDetailsComponent implements OnInit {

  customerDetails: any;
  customerData: any;
  activeId: any;

  constructor(private adminService: AdminService,
    private router: Router,
    private alertService: AlertService,
    private dynamicScriptLoader: DynamicScriptLoaderServiceService) { }

  ngOnInit() {

  // FETCH FROM API
    this.adminService.getCustomerDetails()
      .subscribe(details => {
        if (details.statusCode === 200) {
          // console.log(details);
          this.customerDetails = details;
          this.customerData = this.customerDetails.data;
          //console.log(this.customerData);
          localStorage.setItem('customerdet', JSON.stringify(details.data));
        }
      });
  }

  deleteDealerId(id) {
    this.activeId = id;
    //console.log(this.activeId);
  }

  deletedealer(): void {
    // console.log(this.activeId);

    this.adminService.deleteCustomerDetails(this.activeId)
      .subscribe(res => {
        // console.log(this.customerData);
        this.customerData = this.customerData.filter(cust => cust._id != this.activeId);
        // console.log(this.customerData);
      },
        error => {

          //console.log('error', error);
          this.alertService.error(error);

        });
  }

  //TO LOAD CUSTOM JS DYNAMICALLY

  ngAfterViewInit() {
    this.loadScripts();
  }

  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service

    this.dynamicScriptLoader.load('jqueryDatatables','customTableJs').then(data => {
      //console.log(data);
      // Script Loaded Successfully
    }).catch(error => console.log(error));
  }
}