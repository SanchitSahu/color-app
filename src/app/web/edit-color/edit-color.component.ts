import { DynamicScriptLoaderServiceService } from './../../shared/service/dynamic-script-loader-service.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from './../../shared/service/admin.service';
import { AlertService } from '../../shared/service/alert.service';
import { AuthenticationService } from '../../shared/service/authentication.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-color',
  templateUrl: './edit-color.component.html',
  styleUrls: ['./edit-color.component.css']
})
export class EditColorComponent implements OnInit {

  para;
  activeId;
  editcolors:any;
  editcolordata:any;
  editColorForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
    private dynamicScriptLoader: DynamicScriptLoaderServiceService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private adminService: AdminService,
  ) {
    route.params.subscribe(value => {
      this.para = this.route.snapshot.paramMap.get('id');
     // console.log(this.para);

      this.adminService.getColorDetails(this.para)
        .subscribe(res => {
        //  console.log(res);
          this.editcolors = res;
          this.editcolordata= this.editcolors.data;
          //console.log(this.editcolordata[0]);
        });
    });
  }


  ngOnInit() {
    this.editColorForm = this.formBuilder.group({
      id: ['',Validators.required],
      name: ['', Validators.required],
      colorCode: ['', Validators.required]
    });
  }

  ngAfterViewInit() {
    this.loadScripts();
  }

  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service
  
      this.dynamicScriptLoader.load('datedropperJs','daterangeJs','jqueryminicolors','customPickerJs','spectrumJs').then(data => {
        //console.log(data);
        // Script Loaded Successfully
      }).catch(error => console.log(error));
  }

get f() { return this.editColorForm.controls; }
// 
updateColor(id: number,name: String, colorCode: String): void {
  console.log(id,name, colorCode);
   if(window.confirm('Are you sure, you want to update?')){
  this.adminService.editColor({ id, name, colorCode})
    .subscribe(data => {
      console.log(data);
      this.editcolordata.push(data);
      this.router.navigate(['/color']);
    })
}
}
}












// Id(id) {
//   this.activeId = id;
//   //console.log(this.activeId);
// }

//To get selected data for edit, filter() is used to filter the selected id
// editcolorid(): void
// {
//   this.adminService.getColorDetails(this.activeId)
// .subscribe(color =>{
//   //if(color.statusCode === 200){
//  // console.log(color);
//  // this.colors = color;
//   this.colorsdata = this.colors.data.filter(cust => cust._id != this.activeId);
//   console.log(this.colorsdata);
//   }
// );

// this.adminService.getColorDetails(this.activeId)
    // .subscribe(color =>{
    //   //if(color.statusCode === 200){
    //  // console.log(color);
    //  // this.colors = color;
    //   this.colorsdata = this.colors.data.filter(cust => cust._id != this.activeId);
    //   console.log(this.colorsdata);
    //   }
    // );
// On Edit Button Click
  // onSubmit() {
  //   //  console.log(this.f.username.value, this.f.password.value);
  //         this.submitted = true;
  
  //         // stop here if form is invalid
  //         if (this.editColorForm.invalid) {
  //             return;
  //         }
  
  //         this.loading = true;
  //         this.adminService. editColor(this.color)
  //             .subscribe(res=>{
  //               console.log('response', res);
  //               if (res.statusCode === 200) {
  //                 console.log('resp', res.data);
  //                                 //alert("Login successful");
  //                                 //localStorage.setItem('currentUser', JSON.stringify(res.data));
  //                                   console.log('logged In')
  //                                   //alert('navigate');
  //                                 this.router.navigate(['/color']);
  //                             }
  //                         },
  //                             error => {
  //                               console.log('error', error);
  //                                 this.alertService.error(error);
  //                                 this.loading = false;
  //                             });
  //     }  


