import { DynamicScriptLoaderServiceService } from './../../shared/service/dynamic-script-loader-service.service';
import { AlertService } from './../../shared/service/alert.service';
import { AdminService } from './../../shared/service/admin.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-service-request',
  templateUrl: './service-request.component.html',
  styleUrls: ['./service-request.component.css']
})
export class ServiceRequestComponent implements OnInit {

  serviceRequest: any;
  servicedata: any;
  activeId;
  constructor(private adminService: AdminService,
    private alertService: AlertService,
    private dynamicScriptLoader: DynamicScriptLoaderServiceService) { }

  ngOnInit() {
    this.adminService.getServiceRequest()
      .subscribe(service => {
        if (service.statusCode === 200) {
          //console.log(service);
          this.serviceRequest = service;
          this.servicedata = this.serviceRequest.data;
          //console.log(this.servicedata);
         // localStorage.setItem('servicerequest', JSON.stringify(service.data));

        }
      });
  }

  deleteServiceRequestId(id) {
    this.activeId = id;
    //console.log(this.activeId);
  }

  deleteServiceRequest(): void {
    //console.log(this.activeId); 
    this.adminService.deleteServiceRequest(this.activeId)
      .subscribe(res => {
        //console.log(this.servicedata);
        this.servicedata = this.servicedata.filter(cust => cust._id != this.activeId);
        //console.log(this.servicedata);
      },
        error => {

          console.log('error', error);
          this.alertService.error(error);

        });
  }

   //TO LOAD CUSTOM JS DYNAMICALLY

   ngAfterViewInit() {
    this.loadScripts();
  }

  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service

    this.dynamicScriptLoader.load('jqueryDatatables','customTableJs').then(data => {
      //console.log(data);
      // Script Loaded Successfully
    }).catch(error => console.log(error));
  }

  // addServiceRequests(name: String, email: String, phoneNumber: number, pincode: number, requestType: String): void {
    
  //   this.adminService.addServiceRequest({ name, email, phoneNumber, pincode, requestType})
  //     .subscribe(data => {
  //       console.log(this.servicedata);
  //       this.servicedata.push(data);
        // this.jsondata= data.reverse();
        //this.jsondata.unshift(data);
     // ()=> alert ("Item Deleted");
  //     })
  // }
  
}


