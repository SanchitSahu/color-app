import { DynamicScriptLoaderServiceService } from './../../shared/service/dynamic-script-loader-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from './../../shared/service/admin.service';
import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../shared/service/alert.service';
import { AuthenticationService } from '../../shared/service/authentication.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-addcolor',
  templateUrl: './addcolor.component.html',
  styleUrls: ['./addcolor.component.css']
})
export class AddcolorComponent implements OnInit {

  addColorForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  color = [];

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private adminService: AdminService,
    private dynamicScriptLoader: DynamicScriptLoaderServiceService) { }

  ngOnInit() {
    this.addColorForm = this.formBuilder.group({
      name: ['', Validators.required],
      colorCode: ['', Validators.required]
    });

  }
  //TO LOAD CUSTOM JS DYNAMICALLY

  ngAfterViewInit() {
    this.loadScripts();
  }

  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service

    this.dynamicScriptLoader.load('datedropperJs', 'daterangeJs', 'jqueryminicolors', 'customPickerJs', 'spectrumJs').then(data => {
      //console.log(data);
      // Script Loaded Successfully
    }).catch(error => console.log(error));
  }

  get f() { return this.addColorForm.controls; }

  // ADD COLOR
  addColor(name: string, colorCode: string): void {
    this.adminService.addColor(name, colorCode)
      .subscribe(res => {
        console.log('response', res);
        this.color.push(res);
        if (res.statusCode === 200) {
          console.log('resp', res.data);
          //alert("Login successful");
          // localStorage.setItem('currentUser', JSON.stringify(res.data));
          console.log('Color added successfully');
          //alert('navigate');
          this.router.navigate(['/color']);
        }
      });
  }
}
