import { DynamicScriptLoaderServiceService } from './../../shared/service/dynamic-script-loader-service.service';
import { AlertService } from './../../shared/service/alert.service';
import { AdminService } from './../../shared/service/admin.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  contactUs: any;
  contactdata: any;
  activeId;

  constructor(private adminService: AdminService,
    private alertService: AlertService,
    private dynamicScriptLoader: DynamicScriptLoaderServiceService) { }

  ngOnInit() {

  // Fetch from API
    this.adminService.getContactUs()
      .subscribe(contact => {
        if (contact.statusCode === 200) {
          // console.log(contact);
          this.contactUs = contact;
          this.contactdata = this.contactUs.data;
          // console.log(this.contactdata);
          //  localStorage.setItem('contactus', JSON.stringify(contact.data));

        }
      });
  }

  // ACTIVE ID

  deleteContactUsId(id) {
    this.activeId = id;
    // console.log(this.activeId);
  }

  // DELETE SELECTED CONTACTUS

  deleteContactUs(): void {
    // console.log(this.activeId);
    this.adminService.deleteContactUsData(this.activeId)
      .subscribe(contact => {
        // console.log(this.contactdata);
        this.contactdata = this.contactdata.filter(cont => cont._id != this.activeId);
        // console.log(this.contactdata)
      },
        error => {
          console.log('error', error);
          this.alertService.error(error);
        });
  }

  //TO LOAD CUSTOM JS DYNAMICALLY

  ngAfterViewInit() {
    this.loadScripts();
  }

  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service

    this.dynamicScriptLoader.load('jqueryDatatables','customTableJs').then(data => {
      //console.log(data);
      // Script Loaded Successfully
    }).catch(error => console.log(error));
  }
}
