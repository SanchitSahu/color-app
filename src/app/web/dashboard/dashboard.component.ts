import { AdminService } from './../../shared/service/admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from './../../shared/service/authentication.service';
import { Component, OnInit } from '@angular/core';
import { DynamicScriptLoaderServiceService } from './../../shared/service/dynamic-script-loader-service.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  dashboard: any;

  constructor(private authenticationservice: AuthenticationService,
    private router: Router,
    private adminService: AdminService,
    private dynamicScriptLoader: DynamicScriptLoaderServiceService
     ) {
  
   }

  ngOnInit() {
  
    // FETCH FROM API
      this.adminService.dashboardItems().subscribe(res =>  {
      //  console.log(res);
      this.dashboard = res;
     // console.log(this.dashboard.data);
      });
  }

}
