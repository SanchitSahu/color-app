import { AlertService } from '../shared/service/alert.service';
import { AuthenticationService } from '../shared/service/authentication.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {first} from 'rxjs/operators';
import { DynamicScriptLoaderServiceService } from '../shared/service/dynamic-script-loader-service.service';
//import { NgFlashMessageService } from 'ng-flash-messages';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  username;

  constructor(
   // private ngFlashMessageService: NgFlashMessageService,
   private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private dynamicScriptLoader: DynamicScriptLoaderServiceService
  ) {
    // if (this.authenticationService.currentUserValue) { 
    // this.router.navigate(['/']);
    // }
  }
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    // get return url from route parameters or default to '/'
    // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  // // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }
  onSubmit() {
  //  console.log(this.f.username.value, this.f.password.value);
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        this.authenticationService.login(this.f.username.value, this.f.password.value)
            .subscribe(res=>{
             // console.log('response', res);
              if (res.statusCode === 200) {
               // console.log('resp', res.data);
                                //alert("Login successful");
                                localStorage.setItem('currentUser', JSON.stringify(res.data));
                                  console.log('logged In')
                                  //alert('navigate');
                                this.router.navigate(['/dashboard']);
                            }
               else if(res.statusCode === 201){
               // this.toastr.success('Invalid password');
                alert("Incorrect password");
              //  this.ngFlashMessageService.showFlashMessage({
              //   // Array of messages each will be displayed in new line
              //   messages: ["Incorrect password"]
               
             // });
                           }
                        });
    }

    ngAfterViewInit() {
      this.loadScripts();
    }
    private loadScripts() {
      // You can load multiple scripts by just providing the key as argument into load method of the service
      this.dynamicScriptLoader.load('commomnJS').then(data => {
        console.log(data);
        // Script Loaded Successfully
       
      }).catch(error => console.log(error));
    }
    }