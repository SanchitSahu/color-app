import { DynamicScriptLoaderServiceService } from './../shared/service/dynamic-script-loader-service.service';
import { AuthenticationService } from './../shared/service/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

 
@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.css']
})
export class ShellComponent implements OnInit {

  constructor(private authenticationService: AuthenticationService,
    private router:Router,
    private dynamicScriptLoader: DynamicScriptLoaderServiceService) { }

  ngOnInit() {
    //this.loadScripts();
  }

  ngAfterViewInit() {
    this.loadScripts();
  }

  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service
  
      this.dynamicScriptLoader.load('pCoded','veriticalJs').then(data => {
        //console.log(data);
        // Script Loaded Successfully
      }).catch(error => console.log(error));
  }

  logout(){
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

}
