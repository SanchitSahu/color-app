import { LocatorDealerComponent } from './../web/locator-dealer/locator-dealer.component';
import { EditColorShadeComponent } from './../web/edit-color-shade/edit-color-shade.component';
import { EditColorComponent } from './../web/edit-color/edit-color.component';
import { ContactUsComponent } from '../web/contact-us/contact-us.component';
import { CustomerDetailsComponent } from '../web/customer-details/customer-details.component';
import { NgModule } from '@angular/core';
import { AuthGuard } from '../shared/guards/auth.guard';
import { Routes, RouterModule } from '@angular/router';
import { ChangePasswordComponent } from '../web/change-password/change-password.component';
import { ColorShadeComponent } from '../web/color-shade/color-shade.component';
import { AddcolorComponent } from '../web/addcolor/addcolor.component';
import { AddColorShadeComponent } from '../web/add-color-shade/add-color-shade.component';
import { CmsPagesComponent } from '../web/cms-pages/cms-pages.component';
import { ColorComponent } from '../web/color/color.component';
import { ServiceRequestComponent } from '../web/service-request/service-request.component';
import { DashboardComponent } from '../web/dashboard/dashboard.component';
import { PartiColorShadeComponent } from '../web/parti-color-shade/parti-color-shade.component';

export const FRONTEND_ROUTES: Routes = [
    // { path: '', component: DashboardComponent, canActivate: [AuthGuard] },
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
    { path: 'customerdetails', component: CustomerDetailsComponent, canActivate: [AuthGuard]},
    { path: 'contactus', component: ContactUsComponent, canActivate: [AuthGuard]},
    { path: 'servicerequest', component: ServiceRequestComponent, canActivate: [AuthGuard]},
    { path: 'color', component: ColorComponent, canActivate: [AuthGuard]},
    { path: 'color/addcolor', component: AddcolorComponent, canActivate: [AuthGuard]},
    { path: 'colorshade', component: ColorShadeComponent, canActivate: [AuthGuard]},
    { path: 'colorshade/addcolorshade', component: AddColorShadeComponent, canActivate: [AuthGuard]},
    { path: 'cmspages', component: CmsPagesComponent, canActivate: [AuthGuard]},   
    { path: 'locatedealer', component: LocatorDealerComponent, canActivate: [AuthGuard]},
   // { path: 'colorshade/editcolorshade/id', component: EditColorShadeComponent, canActivate: [AuthGuard]}, 
    { path: 'changepassword', component: ChangePasswordComponent, canActivate: [AuthGuard]}, 
   
   { path:'color/:id', component: PartiColorShadeComponent,  canActivate: [AuthGuard]},
    { path:'color/edit/:id', component: EditColorComponent,  canActivate: [AuthGuard]},
    { path:'colorshade/:id/single', component: EditColorShadeComponent,  canActivate: [AuthGuard]}, 
    

    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(FRONTEND_ROUTES)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
