import { EditColorComponent } from './web/edit-color/edit-color.component';
import { ColorShadeComponent } from './web/color-shade/color-shade.component';
import { CmsPagesComponent } from './web/cms-pages/cms-pages.component';
import { ColorComponent } from './web/color/color.component';
import { ChangePasswordComponent } from './web/change-password/change-password.component';
import { ServiceRequestComponent } from './web/service-request/service-request.component';
import { AddcolorComponent } from './web/addcolor/addcolor.component';
import { AddColorShadeComponent } from './web/add-color-shade/add-color-shade.component';
import { DashboardComponent } from './web/dashboard/dashboard.component';
import { ErrorInterceptor } from './shared/helpers/error.interceptor';
import { JwtInterceptor } from './shared/helpers/jwt.interceptor';
import { AppRoutingModule } from './app.routing';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ShellComponent } from './shell/shell.component';
//import { NgFlashMessagesModule } from 'ng-flash-messages';
import { CustomerDetailsComponent } from './web/customer-details/customer-details.component';
import { ContactUsComponent } from './web/contact-us/contact-us.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditColorShadeComponent } from './web/edit-color-shade/edit-color-shade.component';
import { FilterdataPipe } from './shared/filterdata.pipe';
import { PartiColorShadeComponent } from './web/parti-color-shade/parti-color-shade.component';
import { LocatorDealerComponent } from './web/locator-dealer/locator-dealer.component';
//import { ShowHidePasswordModule } from 'ngx-show-hide-password';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ServiceRequestComponent,
    ColorComponent,
    ColorShadeComponent,
    CmsPagesComponent,
    AddcolorComponent,
    AddColorShadeComponent,
    LoginComponent,
    ChangePasswordComponent,
    ShellComponent,
    CustomerDetailsComponent,
    ContactUsComponent,
    EditColorComponent,
    EditColorShadeComponent,
    FilterdataPipe,
    PartiColorShadeComponent,
    LocatorDealerComponent
    
    
    //ShowHidePasswordModule
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
   AppRoutingModule,
  // NgFlashMessagesModule.forRoot()
  BrowserAnimationsModule,
  ToastrModule.forRoot() 
  ],
  providers: [
    // { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
],
  bootstrap: [AppComponent]
})
export class AppModule { }
