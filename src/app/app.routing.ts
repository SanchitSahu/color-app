import { FRONTEND_ROUTES } from './shell/routing';
import { ShellComponent } from './shell/shell.component';
import { AuthGuard } from './shared/guards/auth.guard';
import { LoginComponent } from './login/login.component';
import { Component, NgModule} from '@angular/core';
import { Routes, RouterModule} from '@angular/router';


const routes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    {
      path: '',
      component: ShellComponent,
      children: FRONTEND_ROUTES
    },
   
];

@NgModule({
    declarations: [],
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}